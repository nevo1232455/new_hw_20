﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Text.RegularExpressions;
using System.Media;


namespace CoolCardGame
{
    public partial class Form1 : Form
    {
        public static string Dsend = "";
        public static int f = 0;

        System.Media.SoundPlayer s = new System.Media.SoundPlayer();

        public Form1()
        {
            InitializeComponent();
            s.SoundLocation = "win.wav";
            s.Play();

            Thread StartTheGame = new Thread(gServerMassege);
            StartTheGame.Start();
            button1.Enabled = false;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Point LocationBlueC = new Point(624, 100);
            Image image = new Bitmap(@"cool-background.jpg");
            this.BackgroundImage = image;
           
            pictureBox1.Image = global::CoolCardGame.Properties.Resources.card_back_blue;
            pictureBox1.Location = LocationBlueC;
            pictureBox1.Size = new System.Drawing.Size(110, 150);
            pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Controls.Add(pictureBox1);



            Point ScoreLocation = new Point(12, 9);
            Point SomeoneScoreLocation = new Point(12, 35);
            Point LocationButton = new Point(622, 35);

            //creat quit
            button1.Text = "FOLDET";
            button1.Location = LocationButton;
            button1.Size = new System.Drawing.Size(115, 38);

            //creat "your score"
            label1.Name = "yourSource";
            label1.Text = "Your Source: ";
            label1.Size = new System.Drawing.Size(100, 15);
            label1.Location = ScoreLocation;

            label2.Name = "competitorSource";
            label2.Text = "Competitor Source: ";
            label2.Size = new System.Drawing.Size(110, 15);
            label2.Location = SomeoneScoreLocation;
        }

        private void GenerateCards()
        {
            int x = 50, y = 550;
            Random cardGenerator;

            Point LocationRedC = new Point(x, y);
            Point LocationRandomC = new Point(615, 50);
            

            //creat 10 red card
            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox RedCard = new PictureBox();
                RedCard.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                RedCard.Image = global::CoolCardGame.Properties.Resources.card_back_red;
                RedCard.Location = LocationRedC;
                RedCard.Size = new System.Drawing.Size(110, 150);
                RedCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                LocationRedC.X += x + 80;

                // assign an event to it
                RedCard.Click += delegate(object sender1, EventArgs e1)
                {

                    cardGenerator = new Random();
                    int cardnum = cardGenerator.Next(1, 14);
                    int cardtype = cardGenerator.Next(1, 5);
                    string resourcename = "_";
                    resourcename += cardnum.ToString();
                    resourcename += cardtype.ToString();
                    object O1 = global::CoolCardGame.Properties.Resources.ResourceManager.GetObject(resourcename);
                    ((PictureBox)sender1).Image = (Image)O1;

                    char c = ' ';
                    if (cardtype == 1)
                    {
                        c = 'C';
                    }
                    else if (cardtype == 2)
                    {
                        c = 'D';
                    }
                    else if (cardtype == 3)
                    {
                        c = 'H';
                    }
                    else if (cardtype == 4)
                    {
                        c = 'S';
                    }
                    if (cardnum < 10)
                    {
                        Dsend = "10" + cardnum.ToString() + c;
                    }
                    else
                    {
                        Dsend = '1' + cardnum.ToString() + c;
                    }
                    f = 1;
                };
                this.Controls.Add(RedCard);
            }
           
        }


        public void gServerMassege()
        {
            int playerscore = 0, vsplayerscore = 0;
            int byteread, byteread2;
            string input = "";
            string inputN, sendNum;

            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            NetworkStream clientStream = client.GetStream();

            byte[] buffer = new byte[4], buffer2 = new byte[4];

            while (input != "0000")
            {
                byteread = clientStream.Read(buffer, 0, 4);
                input = new ASCIIEncoding().GetString(buffer);
            }

            Invoke((MethodInvoker)delegate {button1.Enabled = true; GenerateCards(); });

            while (true)
            {
                if (f != 3)
                {
                    Invoke((MethodInvoker)delegate { label1.Text = "player score: " + playerscore; label2.Text = "opposite player score: " + vsplayerscore; });
                }
                inputN = "";
                sendNum = "";

                if (f == 1)
                {

                    buffer = new ASCIIEncoding().GetBytes(Dsend);
                    clientStream.Write(buffer, 0, 4);
                    clientStream.Flush();
                    f = 0;

                }
                else if (clientStream.DataAvailable)
                {
                    byteread2 = clientStream.Read(buffer2, 0, 4);
                    input = new ASCIIEncoding().GetString(buffer2);
                    string resourcename = "_";
                    if (input[1] == '0')
                    {
                        resourcename += input[2];
                    }
                    else
                    {
                        resourcename += input[1];
                        resourcename += input[2];
                    }

                    char c = ' ';
                    if (input[3] == 'C')
                    {
                        c = '1';
                    }
                    else if (input[3] == 'D')
                    {
                        c = '2';
                    }
                    else if (input[3] == 'H')
                    {
                        c = '3';
                    }
                    else if (input[3] == 'S')
                    {
                        c = '4';
                    }
                    resourcename += c;
                    object O = global::CoolCardGame.Properties.Resources.ResourceManager.GetObject(resourcename);
                    pictureBox1.Image = (Image)O;
                    pictureBox1.Size = new System.Drawing.Size(110, 150);
                    pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                }
                else if (f == 3)
                {
                    buffer = new ASCIIEncoding().GetBytes("2000");
                    clientStream.Write(buffer, 0, 4);
                    clientStream.Flush();
                    exit_game(playerscore, vsplayerscore);
                    break;
                }

                if (input != "")
                {
                    if (input[0] == '2')
                    {
                        exit_game(playerscore, vsplayerscore);

                    }
                }



                if (input != "" && input != "0000" && Dsend != "")
                {
                    if (input != Dsend)
                    {
                        if (input[1] == '0')
                        {
                            inputN += input[2];
                        }
                        else
                        {
                            inputN += input[1];
                            inputN += input[2];
                        }
                        if (Dsend[1] == '0')
                        {
                            sendNum += Dsend[2];
                        }
                        else
                        {
                            sendNum += Dsend[1];
                            sendNum += Dsend[2];
                        }
                        if (inputN == "1")
                        {
                            inputN = "14";
                        }
                        if (sendNum == "1")
                        {
                            sendNum = "14";
                        }
                        
                        int numin = Int32.Parse(inputN), numsend = Int32.Parse(sendNum);
                        
                        if (numsend > numin)
                        {
                            playerscore++;
                        }
                        else
                        {
                            vsplayerscore++;
                        }
                        input = "";
                        Dsend = "";
                        object O = global::CoolCardGame.Properties.Resources.card_back_blue;
                        pictureBox1.Image = (Image)O;
                        pictureBox1.Size = new System.Drawing.Size(110, 150);
                        pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                        f = 4;
                    }

                }
            }


        }


       

        public PictureBox sender1 { get; set; }

        public void exit_game(int pscore, int vscore)
        {
          
            MessageBox.Show("your score: " + pscore + "Not Your score: " + vscore);
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}